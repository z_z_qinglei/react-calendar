import React, { Component } from 'react';
import moment from 'moment';
import './calendar.scss';

class Calendar extends Component {

    constructor() {
        super()
        this.state = {
            dateObject: moment(),
            allMonths: moment.months(),
            showCalendar: false
        }
    }

    // close calendar when user click else where
    hideCalendarOnBlur = (e) => {
        if (e.target.closest(".calendar-input") || e.target.closest(".calendar-dropdown")) {
            return
        } else {
            this.setState({
                isCalendarShown: false
            })
        }
    }

    componentDidMount() {
        document.addEventListener("click", (e) => this.hideCalendarOnBlur(e));
    }

    // clean up eventlistener
    componentWillUnmount() {
        document.removeEventListener("click", (e) => this.hideCalendarOnBlur(e));
    }


    // used when click date input - shows the calendar
    showCalendar = (show) => {
        this.setState({
            isCalendarShown: show
        })
    }

    // change month on click arrow prev and next
    updateMonth = (change) => {
        const month = this.state.dateObject.month()
        const dateObject = moment(this.state.dateObject).set("month", month + change);
        this.setState({
            dateObject,
            isCalendarShown: true
        })
    }

    // change date when click on any date
    selectDate = (evt) => {
        const cell = evt.target
        if (!!cell) {
            const selectedDay = cell.dataset.day
            const dateObject = moment(this.state.dateObject).set("date", selectedDay)

            this.setState({
                dateObject,
                isCalendarShown: false
            })
        }
    }

    /** --------------------------------------------
     * calendar construction
     */
    weekdayShortName = () => {
        let weekdayshort = moment.weekdaysShort();
        return weekdayshort.map(day => {
            return (
                <th key={day} className="week-day">
                    {day}
                </th>
            );
        });
    }

    daysInMonth = () => {
        let dateObject = this.state.dateObject;
        return moment(dateObject).startOf("month").daysInMonth()
    }

    firstDayOfMonth = () => {
        let dateObject = this.state.dateObject;
        let firstDay = moment(dateObject)
            .startOf("month")
            .format("d");
        return firstDay;
    };

    currentDay = () => {
        return this.state.dateObject.format("D");
    };

    currentMonthYear = () => {
        return this.state.dateObject.format("MMMM YYYY");
    };

    formatSelectedDate = () => {
        return this.state.dateObject.format("DD MMM YYYY")
    }

    displayTotalCells = () => {
        // empty cells bef first day
        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            blanks.push(
                <td key={`empty_cell_${i}`} className="calendar-day empty">{""}</td>
            );
        }

        // days in month
        let daysInMonth = [];
        for (let d = 1; d <= this.daysInMonth(); d++) {
            let currentDay = d === parseInt(this.currentDay()) ? "today" : "";
            daysInMonth.push(
                <td key={`day_${d}`} data-day={d} className={`calendar-day ${currentDay}`}>
                    {d}
                </td>
            );
        }

        // total cells
        var totalSlots = [...blanks, ...daysInMonth];
        let rows = [];
        let cells = [];

        totalSlots.forEach((day, i) => {
            if (i % 7 !== 0) {
                cells.push(day); // if index not equal 7 that means not go to next week
            } else {
                rows.push(cells); // when reach next week we contain all td in last week to rows
                cells = []; // empty container
                cells.push(day); // in current loop we still push current row to new container
            }
            if (i === totalSlots.length - 1) { // when end loop we add remain date
                rows.push(cells);
            }
        });

        return (

            <table className="calendar-table" onClick={this.selectDate}>
                <thead>
                    <tr>{this.weekdayShortName()}</tr>
                </thead>
                <tbody>{
                    rows.map((w, i) => {
                        return <tr key={`week_${i}`}>{w}</tr>;
                    })}
                </tbody>
            </table>)
    }

    render() {
        const { isCalendarShown } = this.state
        return (
            <div className="calendar-wrapper">
                <input className="calendar-input" id="calendar-input" placeholder="Select a date"
                    onFocus={() => this.showCalendar(true)}
                    value={this.formatSelectedDate()} readOnly ></input>
                <div hidden={!isCalendarShown} className="calendar-dropdown" id="calendar-dropdown">
                    <div className="calender-toolbar">
                        <span className="calendar-label">
                            {this.currentMonthYear()}
                        </span>
                        <button onClick={() => this.updateMonth(-1)}> {`<`} </button>
                        <button onClick={() => this.updateMonth(1)}> {`>`} </button>
                    </div>
                    {this.displayTotalCells()}

                </div>
            </div>
        )

    }
}
export default Calendar;
